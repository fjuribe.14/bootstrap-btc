<img src="https://miro.medium.com/max/320/0*_rAD9NgK7l6KSlNc.png" alt="Bootstrap logo" width="100" />

## Bootstrap BTC

Es un tema basado en **Bootstrap** para Banking Technologies Consulting

####Instalación

`$ npm i bootstrap-btc`

####Uso

importar en el **main.scss**

    @import "~bootstrap-btc/dist/main.css";
	// o
	@import "~bootstrap-btc/dist/main.scss";